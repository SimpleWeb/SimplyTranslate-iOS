# SimplyTranslate iOS
A Native SwiftUI Client for SimplyTranslate

<img src="https://codeberg.org/SimpleWeb/SimplyTranslate-iOS/raw/branch/main/Screenshots/welcome.png" width="150">
<img src="https://codeberg.org/SimpleWeb/SimplyTranslate-iOS/raw/branch/main/Screenshots/preferences.png" width="150">

## How can I install this?
As of right now, you'll have to build it using Xcode and then just build it onto your device using your own AppleID.
I hope to publish this App to the Apple App Store as soon as possible, but I don't know how to that financially yet, as I do not want to spend $99 per year on an Apple Developer account, if you have any other means to publish an App for Free on the Apple App Store, please let me know about your suggestion at simpleweb@metalune.xyz (Email)

## License
This work is published under the MIT Licens
