//
//  PlaceholderTextEditor.swift
//  SimplyTranslate
//
//  Created by github.com/iarpits
//  https://gist.github.com/iarpits/69a6b10b37944210750b4e4f2a60314f
//  modified by metalune

import SwiftUI

struct PlaceHolderTextEditor: View {
    let placeholder: String
    @Binding var text: String
    var body: some View {
        ZStack(alignment: Alignment(horizontal: .leading, vertical: .top)) {
            if text.isEmpty {
                // We need to add a space before any placeholder text for some reason so it doesn't appear offset
                Text(" \(placeholder)")
                    .foregroundColor(Color(.label))
                    .padding(.top, 10)
            }
            TextEditor(text: $text)
                .opacity(text.isEmpty ? 0.6 : 1)
        }
        .padding([.leading, .trailing], 8)
        .overlay(
            RoundedRectangle(cornerRadius: 6)
                .stroke(Color(.systemGray5), lineWidth: 1.0)
        )
    }
}

