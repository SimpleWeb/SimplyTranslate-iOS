//
//  PreferencesView.swift
//  SimplyTranslate
//
//  Created by metalune on 2/2/22.
//

import SwiftUI

struct PreferencesView: View {
    @State private var showingAlert = false
    
    var body: some View {
        ZStack {
            Form {
                Section() {
                    Button(action: {
                        showingAlert = true
                        /*
                        //TODO: reset saved options
                        let resetAlert = UIAlertController(title: "Reset All", message: "All saved/cached information such as instances and language lists will be lost.", preferredStyle: UIAlertController.Style.alert)
                        
                        resetAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler : { (action: UIAlertAction!) in
                            let domain = Bundle.main.bundleIdentifier!
                            UserDefaults.standard.removePersistentDomain(forName: domain)
                            UserDefaults.standard.synchronize()
                            //print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
                        }))
                        
                        resetAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                            return;
                        }))

                        //present(resetAlert, animated: true, completion: nil)
                         */
                    }) {
                        Text("Reset All")
                            .foregroundColor(Color.red)
                    }
                    .alert(isPresented: $showingAlert) {
                        Alert(
                            title: Text("Reset All"),
                            message: Text("All saved/cached information such as instances and language lists will be lost."),
                            primaryButton: .destructive(Text("Delete")) {
                                let domain = Bundle.main.bundleIdentifier!
                                UserDefaults.standard.removePersistentDomain(forName: domain)
                                UserDefaults.standard.synchronize()
                                Translator.fetchInstances(forceFetch: true)
                                Translator.fetchLanguageLists(forceFetch: true)
                            },
                            secondaryButton: .cancel())
                    }
                    
                    
                    Button(action: {
                        Translator.fetchInstances(forceFetch: true)
                    }) {
                        Text("Fetch Instances")
                    }
                    Button(action: {
                        Translator.fetchLanguageLists(forceFetch: true)
                    }) {
                        Text("Fetch Language Lists")
                    }
                }
                
                Section() {
                    List(Translator.instances, id: \.self) { instance in
                            Text(instance)
                    }
                }
            }
            .navigationBarTitle(Text("Preferences"))
        }
    }
}

#if DEBUG
struct PreferencesView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            PreferencesView()
        }
    }
}
#endif
