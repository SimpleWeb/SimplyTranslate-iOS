//
//  Translator.swift
//  SimplyTranslate
//
//  Created by metalune on 2/2/22.
//

import Foundation

class Translator: ObservableObject {
    @Published var translation: String?
    static var sourceLangs = [String]()
    static var sourceLangsDict: [String: String] = [:]
    static var targetLangs = [String]()
    static var targetLangsDict: [String: String] = [:]
    static var instances = [String]()
    static let defaults = UserDefaults.standard
    
    func translate(input_text: String, from: String, to: String) {
        let instance = Translator.instances.randomElement() ?? "simplytranslate.org"
        
        // create the URL
        var urlComps = URLComponents(string: "https://\(instance)/api/translate")!
        urlComps.queryItems = [
            URLQueryItem(name: "engine", value: "google"),
            URLQueryItem(name: "text", value: input_text),
            URLQueryItem(name: "from", value: Translator.sourceLangsDict[from]),
            URLQueryItem(name: "to", value: Translator.sourceLangsDict[to])
        ]
        
        guard let translationURL = urlComps.url else { return }
        
        // fetch the data
        let task = URLSession.shared.dataTask(with: translationURL) {
            data, _, _ in
            guard let data = data else { return }
            guard let text = String(bytes: data, encoding: String.Encoding.utf8) else { return }
            
            DispatchQueue.main.async {
                self.translation = text
            }
        }
        
        task.resume()
    }
    
    static func fetchLanguageLists(forceFetch: Bool) {
        if !forceFetch {
            let savedSourceLangs = UserDefaults.standard.object(forKey: "sourceLangs") as? [String:String] ?? [:]
            let savedTargetLangs = UserDefaults.standard.object(forKey: "targetLangs") as? [String:String] ?? [:]
            
            if savedSourceLangs.keys.count > 0 && savedTargetLangs.keys.count > 0 {
                Translator.sourceLangsDict = savedSourceLangs
                Translator.targetLangsDict = savedTargetLangs
                
                Translator.sourceLangs = Array(Translator.sourceLangsDict.keys).sorted()
                Translator.targetLangs = Array(Translator.targetLangsDict.keys).sorted()
                return
            }
        }
        
        Translator.sourceLangs = [String]()
        Translator.targetLangs = [String]()
        Translator.sourceLangsDict = [:]
        Translator.targetLangsDict = [:]

        let instance = Translator.instances.randomElement() ?? "simplytranslate.org"
        
        guard let sourceURL = URL(string: "https://\(instance)/api/source_languages?engine=google") else { return }
        
        let sourceTask = URLSession.shared.dataTask(with: sourceURL) {
            data, response, _ in
            guard let data = data else { return }
            guard let textData = String(bytes: data, encoding: String.Encoding.utf8) else { return }
            print(textData)
            
            var linesCounted: Int = 0
            var language: String = ""
            
            textData.enumerateLines { (line, _) in
                if linesCounted == 0 {
                    language = line
                } else if linesCounted == 1 {
                    Translator.sourceLangsDict[language] = line
                    Translator.sourceLangs.append(language)
                    linesCounted = -1
                }
                
                linesCounted += 1
            }

            Translator.sourceLangs = Translator.sourceLangs.sorted()
            UserDefaults.standard.set(Translator.sourceLangsDict, forKey: "sourceLangs")
        }
        sourceTask.resume()
        
        
        guard let targetURL = URL(string: "https://\(instance)/api/target_languages?engine=google") else { return }
        
        let targetTask = URLSession.shared.dataTask(with: targetURL) {
            data, response, _ in
            
            guard let data = data else { return }
            guard let textData = String(bytes: data, encoding: String.Encoding.utf8) else { return }
            
            var linesCounted: Int = 0
            var language: String = ""

            textData.enumerateLines { (line, _) in
                if linesCounted == 0 {
                    language = line
                } else if linesCounted == 1 {
                    Translator.targetLangsDict[language] = line
                    Translator.targetLangs.append(language)
                    linesCounted = -1
                }
                
                linesCounted += 1
            }

            Translator.targetLangs = Translator.targetLangs.sorted()
            UserDefaults.standard.set(Translator.targetLangsDict, forKey: "targetLangs")
        }
        targetTask.resume()
    }
    
    /* this is static for now because I want to call this function from other places in the UI other than ContentView
    *  This should be changed in the future once I figure out how to have only one instance of Translator that works beautifully.
    */
    static func fetchInstances(forceFetch: Bool) {
        if !forceFetch {
            let cachedInstances = UserDefaults.standard.object(forKey: "Instances") as? [String] ?? [String]()
            if cachedInstances.count > 0 {
                Translator.instances = cachedInstances
                return
            }
        }
        
        Translator.instances = [String]()
        
        guard let url = URL(string: "https://simple-web.org/instances/simplytranslate") else { return }
        let task = URLSession.shared.dataTask(with: url) {
            data, _, _ in
            guard let data = data else { return }
            guard let textData = String(bytes: data, encoding: String.Encoding.utf8) else { return }
        
            DispatchQueue.main.async {
                Translator.instances = textData.components(separatedBy: "\n").filter({ $0 != "" })
                UserDefaults.standard.set(Translator.instances, forKey: "Instances")
            }
        }
        
        task.resume()
    }
}
