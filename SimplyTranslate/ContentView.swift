//
//  ContentView.swift
//  SimplyTranslate
//
//  Created by metalune on 2/2/22.
//

import SwiftUI



struct ContentView: View {
    @State private var input_text: String = ""
    @StateObject var trans = Translator()
    
    @State private var sourceLang = UserDefaults.standard.object(forKey: "sourceLang") as? String ?? "Autodetect"
    @State private var targetLang = UserDefaults.standard.object(forKey: "targetLang") as? String ?? "English"
    
    init() {
        Translator.fetchInstances(forceFetch: false)
        Translator.fetchLanguageLists(forceFetch: false)
    }

    
     
    var body: some View {
        NavigationView {
            VStack {
                HStack {
                    Spacer()
                    Picker("Source", selection: $sourceLang) {
                        ForEach(Translator.sourceLangs, id: \.self) {
                            Text($0)
                        }
                    }
                    .pickerStyle(.menu)

                    Spacer()

                    Button(action: {
                        if sourceLang != "Autodetect" {
                            let temp = targetLang
                            targetLang = sourceLang
                            sourceLang = temp
                        }
                    }, label: {
                        Image(systemName: "arrow.left.arrow.right")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 28, height: 28)
                            
                    })
                    .disabled(sourceLang == "Autodetect")

                    Spacer()

                    Picker("Target", selection: $targetLang) {
                        ForEach(Translator.targetLangs, id: \.self) {
                            Text($0)
                        }
                    }
                    .pickerStyle(.menu)
                    
                    Spacer()
                }
                .padding()

                PlaceHolderTextEditor(placeholder: "Enter text you want to translate", text: $input_text)
                    .padding()

                if let translation = trans.translation {
                    ScrollView {
                        VStack(alignment: .leading) {
                            Text(translation)
                                .lineLimit(nil)
                                .font(Font.system(size: 25))
                        }
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding()
                    }
                    .padding()

                    Button(action: {
                        let activityNC = UIActivityViewController(activityItems: [translation], applicationActivities: nil)
                        UIApplication.shared.windows.first?.rootViewController?.present(activityNC, animated: true, completion: nil)
                    }) {
                        Image(systemName: "square.and.arrow.up")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 28, height: 28)
                    }
                }
                Button(action: {
                    //TODO: add loading indicator
                    trans.translate(input_text: input_text, from: sourceLang, to: targetLang)
                }, label: {
                    Text("Translate!")
                        .bold()
                        .frame(width: 250, height: 40)
                        .foregroundColor(Color.white)
                        .background(.tint)
                        .cornerRadius(8)
                })
                    .disabled(input_text.isEmpty)
                    .padding()
            }
            .navigationTitle("SimplyTranslate")
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    NavigationLink(destination: PreferencesView()) { Label("Preferences", systemImage: "gear")}
                }
            }
            .onDisappear {
                //TODO: move this somewhere else where it also saves when the app is closed, or rather, as soon as it's selected
                UserDefaults.standard.set(sourceLang, forKey: "sourceLang")
                UserDefaults.standard.set(targetLang, forKey: "targetLang")
            }
        }
    }
}

#if DEBUG
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
        }
    }
}
#endif
