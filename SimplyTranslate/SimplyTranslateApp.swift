
var targetLangsDict: [String: String] = [:]//
//  SimplyTranslateApp.swift
//  SimplyTranslate
//
//  Created by metalune on 2/2/22.
//

import SwiftUI

@main
struct SimplyTranslateApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
